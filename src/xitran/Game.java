/*
 * Copyright (C) 2014 easimer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xitran;

import org.jsfml.graphics.IntRect;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderStates;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.system.Vector2f;
import org.jsfml.window.VideoMode;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class Game {

    RenderWindow window;
    VideoMode mode;

    private Boolean _running = true;

    private static volatile Game instance = null;

    public static synchronized Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    public static synchronized Game newInstance() {
        instance = new Game();
        return instance;
    }

    private Game() {
        this(640, 480, 60, "Xitran");
    }

    private Game(int width, int height) {
        this(width, height, 60, "Xitran");
    }

    private Game(int width, int height, int fpslimit) {
        this(width, height, fpslimit, "Xitran");
    }

    private Game(int width, int height, int fpslimit, String title) {
        window = new RenderWindow();
        mode = new VideoMode(width, height);
        window.create(mode, title);
        window.setFramerateLimit(fpslimit);
    }

    RectangleShape r;
    
    public void Loop() {
        while (_running && window.isOpen()) {
            window.clear();
            if(r == null)
                r = new RectangleShape(new Vector2f(500, 300));
            window.draw(r);
            window.display();
            for (Event event : window.pollEvents()) {
                if (event.type == Event.Type.CLOSED) {
                    //The user pressed the close button
                    window.close();
                }
            }
        }
    }
}
