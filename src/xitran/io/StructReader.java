/*
 * Copyright (C) 2014 easimer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package xitran.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author easimer
 * @param <T> Type
 */
public class StructReader<T> {

    FileInputStream fs;
    public ObjectInputStream is;

    public StructReader(File f) {
        try {
            if (!f.exists()) {
                throw new IllegalArgumentException("File not found: " + f.getCanonicalPath());
            }
            fs = new FileInputStream(f);
            is = new ObjectInputStream(fs);
        } catch (IOException ioe) {
            System.out.println("IO Error");
        }
    }

    public T readObject() {
        try {
            return (T) is.readObject();
        } catch (IOException ex) {
            System.out.println("IO Error");
        } catch (ClassNotFoundException ex) {
            System.out.println("Class not found");
        }
        return null;
    }

    public void close() {
        try {
            is.close();
            fs.close();
        } catch (IOException ex) {
            System.out.println("IO Error");
        }
    }
}
